<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::redirect('/', '/api/auth');

Route::group([
    'middleware' => [
        "check-headers",
        "log-access",
        "check-json-data"
    ]
], function () {
    Route::group([
        'prefix' => 'auth'
    ], function () {
        Route::post('login', 'AuthController@login');
        Route::post('signup', 'AuthController@signup');

        Route::group([
            'middleware' => [
                'check-api-token'
            ]
        ], function () {
            Route::post('logout', 'AuthController@logout');
            Route::post('refresh', 'AuthController@refresh');
        });
    });

    Route::group([
        'prefix' => 'store',
        'middleware' => [
            'check-api-token'
        ]
    ], function () {
        Route::post('register', 'StoreController@register');
        Route::post('update', 'StoreController@update');
        Route::post('delete', 'StoreController@delete');
        Route::post('list', 'StoreController@list');
        Route::post('associate-item', 'StoreController@associateItem');
        Route::post('desassociate-item', 'StoreController@desassociateItem');
        Route::post('register-stored-item-quantity', 'StoreController@registerStoredItemQuantity');
    });

    Route::group([
        'prefix' => 'item',
        'middleware' => [
            'check-api-token'
        ]
    ], function () {
        Route::post('register', 'ItemController@register');
        Route::post('update', 'ItemController@update');
        Route::post('delete', 'ItemController@delete');
        Route::post('list', 'ItemController@list');
    });
});

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
