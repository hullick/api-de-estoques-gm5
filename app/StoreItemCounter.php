<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\Pivot;

class StoreItemCounter extends Pivot
{
    protected $table = "store_item_counter";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'quantity', "user_id", "store_item_id"
    ];

    /**
     * Define relationship between store item and their counter
     *
     * @return BelongsTo
     */
    public function storeItem()
    {
        return $this
            ->belongsTo("App\StoreItem");
    }
}
