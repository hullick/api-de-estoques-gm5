<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Log;

class Logging
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        Log::channel('connections')->info(
            "ORIGIN_IP: " . $request->ip() . " | " .
            "METHOD: " . $request->method() . " | " .
            "REQUEST_TIME: " . now()
        );

        return $next($request);
    }
}
