<?php

namespace App\Http\Middleware;

use Closure;

class CheckValidJsonData
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        json_decode($request->getContent());

        if (json_last_error() != 0) { // Error on decode
            abort(400, 'Os dados do parâmetro não estão corretos. Erro de json: ' . json_last_error_msg());
        }

        return $next($request);
    }
}
