<?php

namespace App\Http\Middleware;

use App\AccessToken;
use Closure;

class CheckApiToken
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->headers->has("api_token")) {
            abort(400, 'O cabeçalho da requisição não possui o item "api_token"');
        }

        /** @var AccessToken $accessToken */
        $accessToken = AccessToken::where("api_token", $request->header("api_token"))
            ->first();

        if (!$accessToken->isValid()) {
            abort(401, 'O token para acesso da api não é válido.');
        }

        return $next($request);
    }
}
