<?php

namespace App\Http\Middleware;

use Closure;

class CheckHeaders
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        #TODO: Add language for translation

        if (!$request->headers->has("Content-Type")) {
            abort(400, 'O cabeçalho da requisição não possui o atributo "Content-Type"');
        } else {
            if ($request->headers->get("Content-Type") !== "application/json") {
                abort(400, 'O atributo "Content-Type" do cabeçalho da requisição não é válido');
            }
        }

        return $next($request);
    }
}
