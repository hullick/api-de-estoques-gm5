<?php

namespace App\Http\Controllers;


use App\AccessToken;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    /**
     * Create user and return a session token
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function signup(Request $request)
    {
        try {
            $result = json_decode($request->getContent(), true);

            Validator::make($result, [
                'name' => 'required|string',
                'email' => 'required|string|email',
                'password' => 'required|string'
            ])->validate();

            $user = new User([
                'name' => $result["name"],
                'email' => $result["email"],
                'password' => Hash::make($result["password"]),
            ]);

            $user->save();

            $user->accessToken()->create([
                'api_token' => Str::random(60),
                'user_id' => $user->id
            ]);


            # TODO: Translate message
            return response()->json([
                "success" => true,
                'message' => 'Usuário criado com sucesso.',
                "code" => 200,
                "data" => [
                    "api_token" => $user->getCurrentAccessToken()->api_token,
                ]
            ], 200);
        } catch (ValidationException $exception) {
            abort(400, 'Os parâmetros da requisição estão incorretos');
        } catch (QueryException $exception) {
            if (strpos($exception->getMessage(), "Duplicate entry") >= 0) {
                abort(400, "O usuário que está para ser registrado no banco de dados já existe.");
            } else {
                abort(400, "Erro no banco de dados, código: " . $exception->getCode() . ".");
            }
        }
    }

    /**
     * Login user and create token
     *
     * @param Request $request
     * @return JsonResponse [string] access_token
     */
    public function login(Request $request)
    {
        $result = json_decode($request->getContent(), true);

        Validator::make($result, [
            'email' => 'required|string|email',
            'password' => 'required|string'
        ])->validate();

        if (!Auth::attempt($result))
            abort(401, 'O usuário não pôde ser autenticado com as informações passadas');

        if ($request->user()->getCurrentAccessToken() == null) {
            // Criar novo token

            $request->user()->accessToken()->create([
                'api_token' => Str::random(60),
                'user_id' => $request->user()->id
            ]);

            return response()->json([
                "success" => true,
                'message' => 'Token da sessão para api do usuário criado com sucesso.',
                "code" => 200,
                "data" => [
                    "api_token" => $request->user()->getCurrentAccessToken()->api_token,
                ]
            ], 200);
        } else {
            abort(409, 'O usuário já possui uma sessão ativa');
        }
    }

    /**
     * Invalidade token
     *
     * @param Request $request
     * @return JsonResponse [string] access_token
     */
    public function logout(Request $request)
    {
        /** @var AccessToken $accessToken */
        $accessToken = AccessToken::where("api_token", $request->header("api_token"))
            ->first();

        if ($accessToken AND $accessToken->isValid()) {
            $newDate = Carbon::createFromFormat('Y-m-d H:i:s', "2000-01-01 00:00:00");

            AccessToken::where("api_token", $request->header("api_token"))
                ->update([
                    'created_at' =>
                        $newDate->toDateTimeString()
                ]);

            return response()->json([
                "success" => true,
                'message' => 'Token da sessão para api do usuário foi invalidada com sucesso.',
                "code" => 200,
                "data" => []
            ], 200);
        } else {
            abort(409, 'O token da sessão para api informada não é válida');
        }
    }

    /**
     * Refresh token
     *
     * @param Request $request
     * @return JsonResponse [string] access_token
     */
    public function refresh(Request $request)
    {
        /** @var AccessToken $accessToken */
        $accessToken = AccessToken::where("api_token", $request->header("api_token"))
            ->first();

        if ($accessToken AND $accessToken->isValid()) {
            AccessToken::where("api_token", $request->header("api_token"))
                ->update([
                    'created_at' =>
                        \Illuminate\Support\Carbon::now()->toDateTimeString()
                ]);

            return response()->json([
                "success" => true,
                'message' => 'Token da sessão para api do usuário renovado com sucesso.',
                "code" => 200,
                "data" => []
            ], 200);
        } else {
            abort(409, 'O token da sessão para api informada não é válida');
        }
    }
}
