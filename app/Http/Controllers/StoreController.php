<?php

namespace App\Http\Controllers;

use App\AccessToken;
use App\Store;
use App\StoreItemCounter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use function Psy\debug;

class StoreController extends Controller
{
    public function register(Request $request)
    {
        try {
            $result = json_decode($request->getContent());

            /** @var AccessToken $accessToken */
            $accessToken = AccessToken::where("api_token", $request->header("api_token"))
                ->first();

            DB::beginTransaction();

            $data = [];

            if (is_array($result->data)) { // Multiple data

                foreach ($result->data as $store) {
                    Validator::make((array)$store, [
                        'name' => 'required|string',
                    ])->validate();

                    array_push($data, Store::create([
                        "name" => $store->name,
                        "user_id" => $accessToken->user_id
                    ]));
                }

            } else { //Single data
                Validator::make((array)$result->data, [
                    'name' => 'required|string',
                ])->validate();

                $data = Store::create([
                    "name" => $result->data->name,
                    "user_id" => $accessToken->user_id
                ]);
            }

            DB::commit();

            # TODO: Translate message
            return response()->json([
                "success" => true,
                'message' => 'Loja(s) registrada(s) com sucesso.',
                "code" => 200,
                "data" => $data
            ], 200);
        } catch (ValidationException $exception) {
            DB::rollBack();

            abort(400, 'Os parâmetros da requisição estão incorretos');
        } catch (QueryException $exception) {
            DB::rollBack();

            if (strpos($exception->getMessage(), "Duplicate entry") !== false) {
                abort(400, "O nome da loja que está para ser registrado no banco de dados já existe.");
            } else {
                abort(400, "Erro no banco de dados, código: " . $exception->getCode() . ".");
            }
        }
    }

    public function update(Request $request)
    {
        try {
            DB::beginTransaction();

            $data = [];

            $result = json_decode($request->getContent());

            if (is_array($result->data)) { // Multiple data

                foreach ($result->data as $store) {
                    Validator::make((array)$store, [
                        'name' => 'required|string',
                        'id' => 'required|numeric'
                    ])->validate();

                    $storeInstance = Store::find($store->id);

                    $storeInstance->name = $store->name;

                    $storeInstance->update([
                        "name" => $store->name,
                    ]);

                    array_push($data, $storeInstance->toArray());
                }

            } else { //Single data
                Validator::make((array)$result->data, [
                    'name' => 'required|string',
                    'id' => 'required|numeric'
                ])->validate();

                $storeInstance = Store::find($result->data->id);

                $storeInstance->name = $result->data->name;

                $storeInstance->update([
                    "name" => $result->data->name,
                ]);

                array_push($data, $storeInstance->toArray());
            }

            DB::commit();

            # TODO: Translate message
            return response()->json([
                "success" => true,
                'message' => 'Loja(s) atualizada(s) com sucesso.',
                "code" => 200,
                "data" => $data
            ], 200);
        } catch (ValidationException $exception) {
            DB::rollBack();

            abort(400, 'Os parâmetros da requisição estão incorretos');
        } catch (QueryException $exception) {
            DB::rollBack();

            abort(400, "Erro no banco de dados, código: " . $exception->getCode() . ".");
        }
    }

    public function delete(Request $request)
    {
        try {
            DB::beginTransaction();

            $result = json_decode($request->getContent());

            if (is_array($result->data)) { // Multiple data

                foreach ($result->data as $store) {
                    Validator::make((array)$store, [
                        'id' => 'required|numeric'
                    ])->validate();

                    $storeInstance = Store::find($store->id);

                    if ($storeInstance !== null)
                        $storeInstance->delete();
                }

            } else { //Single data
                Validator::make((array)$result->data, [
                    'id' => 'required|numeric'
                ])->validate();

                $storeInstance = Store::find($result->data->id);

                if ($storeInstance !== null)
                    $storeInstance->delete();
            }

            DB::commit();

            # TODO: Translate message
            return response()->json([
                "success" => true,
                'message' => 'Loja(s) deletadas(s) com sucesso.',
                "code" => 200,
                "data" => null
            ], 200);
        } catch (ValidationException $exception) {
            DB::rollBack();

            abort(400, 'Os parâmetros da requisição estão incorretos');
        } catch (QueryException $exception) {
            DB::rollBack();

            abort(400, "Erro no banco de dados, código: " . $exception->getCode() . ".");
        }
    }

    /**
     * @param Request $request
     * @return array
     */
    public function list(Request $request)
    {
        return response()->json([
            "success" => true,
            'message' => 'Loja(s) listadas(s) com sucesso.',
            "code" => 200,
            "data" => Store::with("item")->get("*")->toArray()
        ], 200);
    }

    /**
     * @param Request $request
     * @return array
     */
    public function associateItem(Request $request)
    {
        try {
            DB::beginTransaction();

            $data = [];

            $result = json_decode($request->getContent());

            if (is_array($result->data)) { // Multiple data

                foreach ($result->data as $store_item) {
                    Validator::make((array)$store_item, [
                        'store_id' => 'required|numeric',
                        'item_id' => 'required|numeric'
                    ])->validate();

                    /** @var Store $storeInstance */
                    $storeInstance = Store::find($store_item->store_id);

                    $storeInstance->item()->attach($store_item->item_id);

                    $storeInstance->save();
                }

            } else { //Single data
                Validator::make((array)$result->data, [
                    'store_id' => 'required|numeric',
                    'item_id' => 'required|numeric'
                ])->validate();

                /** @var Store $storeInstance */
                $storeInstance = Store::find($result->data->store_id);

                $storeInstance->item()->attach($result->data->item_id);

                $storeInstance->save();
            }

            DB::commit();

            # TODO: Translate message
            return response()->json([
                "success" => true,
                'message' => 'Item(s) associado(s) com sucesso a(s) loja(s).',
                "code" => 200,
                "data" => $data
            ], 200);
        } catch (ValidationException $exception) {
            DB::rollBack();

            abort(400, 'Os parâmetros da requisição estão incorretos');
        } catch (QueryException $exception) {
            DB::rollBack();

            abort(400, "Erro no banco de dados, código: " . $exception->getCode() . ".");
        }
    }

    /**
     * @param Request $request
     * @return array
     */
    public function desassociateItem(Request $request)
    {
        try {
            DB::beginTransaction();

            $data = [];

            $result = json_decode($request->getContent());

            if (is_array($result->data)) { // Multiple data

                foreach ($result->data as $store_item) {
                    Validator::make((array)$store_item, [
                        'store_id' => 'required|numeric',
                        'item_id' => 'required|numeric'
                    ])->validate();

                    /** @var Store $storeInstance */
                    $storeInstance = Store::find($store_item->store_id);

                    $storeInstance->item()->detach($store_item->item_id);

                    $storeInstance->save();
                }

            } else { //Single data
                Validator::make((array)$result->data, [
                    'store_id' => 'required|numeric',
                    'item_id' => 'required|numeric'
                ])->validate();

                /** @var Store $storeInstance */
                $storeInstance = Store::find($result->data->store_id);

                $storeInstance->item()->detach($result->data->item_id);

                $storeInstance->save();
            }

            DB::commit();

            # TODO: Translate message
            return response()->json([
                "success" => true,
                'message' => 'Item(s) desassociado(s) com sucesso a(s) loja(s).',
                "code" => 200,
                "data" => $data
            ], 200);
        } catch (ValidationException $exception) {
            DB::rollBack();

            abort(400, 'Os parâmetros da requisição estão incorretos');
        } catch (QueryException $exception) {
            DB::rollBack();

            abort(400, "Erro no banco de dados, código: " . $exception->getCode() . ".");
        }
    }

    /**
     * @param Request $request
     * @return array
     */
    public function registerStoredItemQuantity(Request $request)
    {
        try {
            DB::beginTransaction();

            /** @var AccessToken $accessToken */
            $accessToken = AccessToken::where("api_token", $request->header("api_token"))
                ->first();

            $data = [];

            $result = json_decode($request->getContent());

            if (is_array($result->data)) { // Multiple data

                foreach ($result->data as $storeItemCounter) {
                    Validator::make((array)$storeItemCounter, [
                        'store_id' => 'required|numeric',
                        'item_id' => 'required|numeric',
                        'quantity' => 'required|numeric'
                    ])->validate();

                    $query = DB::table('store_item')
                        ->select(DB::raw('id'))
                        ->where([
                            'store_id' => $storeItemCounter->store_id,
                            'item_id' => $storeItemCounter->item_id
                        ])
                        ->first();

                    array_push($data, StoreItemCounter::create([
                        'store_item_id' => $query->id,
                        'quantity' => $storeItemCounter->quantity,
                        "user_id" => $accessToken->user_id
                    ])->toArray());
                }

            } else { //Single data
                Validator::make((array)$result->data, [
                    'store_id' => 'required|numeric',
                    'item_id' => 'required|numeric',
                    'quantity' => 'required|numeric'
                ])->validate();

                $query = DB::table('store_item')
                    ->select(DB::raw('id'))
                    ->where([
                        'store_id' => $result->data->store_id,
                        'item_id' => $result->data->item_id
                    ])
                    ->first();

                array_push($data, StoreItemCounter::create([
                    'store_item_id' => $query->id,
                    'quantity' => $result->data->quantity,
                    "user_id" => $accessToken->user_id
                ]));
            }

            DB::commit();

            # TODO: Translate message
            return response()->json([
                "success" => true,
                'message' => 'Quantidade de Item(s) da(s) loja(s) registrado(s) com sucesso.',
                "code" => 200,
                "data" => $data
            ], 200);
        } catch (ValidationException $exception) {
            DB::rollBack();

            abort(400, 'Os parâmetros da requisição estão incorretos');
        } catch (QueryException $exception) {
            DB::rollBack();

            if (strpos($exception->getMessage(), "foreign key constraint fails") !== false) {
                abort(409, "A loja e o item não estão associados");
            } else {
                abort(400, "Erro no banco de dados, código: " . $exception->getCode() . ".");
            }
        }
    }
}
