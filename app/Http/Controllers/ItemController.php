<?php

namespace App\Http\Controllers;

use App\AccessToken;
use App\Item;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class ItemController extends Controller
{
    public function register(Request $request)
    {
        try {
            $result = json_decode($request->getContent());

            /** @var AccessToken $accessToken */
            $accessToken = AccessToken::where("api_token", $request->header("api_token"))
                ->first();

            DB::beginTransaction();

            $data = [];

            if (is_array($result->data)) { // Multiple data

                foreach ($result->data as $item) {
                    Validator::make((array)$item, [
                        'name' => 'required|string',
                        'value' => 'required|numeric'
                    ])->validate();

                    array_push($data, Item::create([
                        "name" => $item->name,
                        "value" => $item->value,
                        "user_id" => $accessToken->user_id
                    ]));
                }

            } else { //Single data
                Validator::make((array)$result->data, [
                    'name' => 'required|string',
                    'value' => 'required|numeric'
                ])->validate();

                $data = Item::create([
                    "name" => $result->data->name,
                    "value" => $result->data->value,
                    "user_id" => $accessToken->user_id
                ]);
            }

            DB::commit();

            # TODO: Translate message
            return response()->json([
                "success" => true,
                'message' => 'Item(s) registrado(s) com sucesso.',
                "code" => 200,
                "data" => $data
            ], 200);
        } catch (ValidationException $exception) {
            DB::rollBack();

            abort(400, 'Os parâmetros da requisição estão incorretos');
        } catch (QueryException $exception) {
            DB::rollBack();

            if (strpos($exception->getMessage(), "Duplicate entry") !== false) {
                abort(400, "O nome da item que está para ser registrado no banco de dados já existe.");
            } else {
                abort(400, "Erro no banco de dados, código: " . $exception->getCode() . ".");
            }
        }
    }

    public function update(Request $request)
    {
        try {
            DB::beginTransaction();

            $data = [];

            $result = json_decode($request->getContent());

            if (is_array($result->data)) { // Multiple data

                foreach ($result->data as $item) {
                    Validator::make((array)$item, [
                        'name' => 'required|string',
                        'value' => 'required|numeric',
                        'id' => 'required|int'
                    ])->validate();

                    $itemInstance = Item::find($item->id);

                    $itemInstance->name = $item->name;
                    $itemInstance->value = $item->value;

                    $itemInstance->update([
                        "name" => $item->name,
                        "value" => $item->value
                    ]);

                    array_push($data, $itemInstance->toArray());
                }

            } else { //Single data
                Validator::make((array)$result->data, [
                    'name' => 'required|string',
                    'value' => 'required|numeric',
                    'id' => 'required|int'
                ])->validate();

                $itemInstance = Item::find($result->data->id);

                $itemInstance->name = $result->data->name;
                $itemInstance->value = $result->data->value;

                $itemInstance->update([
                    "name" => $result->data->name,
                    "value" => $result->data->value
                ]);

                array_push($data, $itemInstance->toArray());
            }

            DB::commit();

            # TODO: Translate message
            return response()->json([
                "success" => true,
                'message' => 'Item(s) atualizado(s) com sucesso.',
                "code" => 200,
                "data" => $data
            ], 200);
        } catch (ValidationException $exception) {
            DB::rollBack();

            abort(400, 'Os parâmetros da requisição estão incorretos');
        } catch (QueryException $exception) {
            DB::rollBack();

            abort(400, "Erro no banco de dados, código: " . $exception->getCode() . ".");
        }
    }

    public function delete(Request $request)
    {
        try {
            DB::beginTransaction();

            $result = json_decode($request->getContent());

            if (is_array($result->data)) { // Multiple data

                foreach ($result->data as $item) {
                    Validator::make((array)$item, [
                        'id' => 'required|int'
                    ])->validate();

                    $itemInstance = Item::find($item->id);

                    if ($itemInstance !== null)
                        $itemInstance->delete();
                }

            } else { //Single data
                Validator::make((array)$result->data, [
                    'id' => 'required|int'
                ])->validate();

                $itemInstance = Item::find($result->data->id);

                if ($itemInstance !== null)
                    $itemInstance->delete();
            }

            DB::commit();

            # TODO: Translate message
            return response()->json([
                "success" => true,
                'message' => 'Item(s) deletado(s) com sucesso.',
                "code" => 200,
                "data" => null
            ], 200);
        } catch (ValidationException $exception) {
            DB::rollBack();

            abort(400, 'Os parâmetros da requisição estão incorretos');
        } catch (QueryException $exception) {
            DB::rollBack();

            abort(400, "Erro no banco de dados, código: " . $exception->getCode() . ".");
        }
    }

    public function list(Request $request)
    {
        return Item::all()->toArray();
    }
}
