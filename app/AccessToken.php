<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * @property string api_token
 * @property Carbon created_at
 * @property int max_duration
 * @property int user_id
 */
class AccessToken extends Model
{
    protected $table = "access_token";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'api_token', 'user_id', 'max_duration', 'created_at'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Check if the access token is valid
     *
     * @return bool
     */
    public function isValid()
    {
        return Carbon::parse($this->created_at)->addSeconds($this->max_duration)->gt(now());
    }

}
