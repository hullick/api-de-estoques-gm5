<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Store extends Model
{
    protected $table = "store";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', "user_id"
    ];

    /**
     * Define relationship between store and their creator
     *
     * @return BelongsTo
     */
    public function user()
    {
        return $this
            ->belongsTo("App\User", "user_id");
    }

    /**
     * Define relationship between item and the store that it belongs
     *
     * @return BelongsToMany
     */
    public function item()
    {
        return $this
            ->belongsToMany("App\Item", "store_item", "store_id", "item_id");
    }
}
