<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Carbon;
use Laravel\Passport\HasApiTokens;

/**
 * @property AccessToken $accessToken
 */
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Define relationship between user and their last access token
     *
     * @return HasMany
     */
    public function accessToken()
    {
        return $this
            ->hasMany("App\AccessToken", "user_id");
    }

    /**
     * Get last valid access token
     *
     * @return AccessToken|null
     */
    public function getCurrentAccessToken()
    {
        /** @var AccessToken $accessToken */
        $accessToken = AccessToken::where([
            ["user_id", $this->id],
        ])
            ->orderBy('created_at', 'desc')->first();

        return $accessToken->isValid() ?
            $accessToken : null;
    }
}
