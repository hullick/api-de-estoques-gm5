<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Item extends Model
{

    protected $table = "item";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', "user_id", "value"
    ];

    /**
     * Define relationship between item and their creator
     *
     * @return BelongsTo
     */
    public function user()
    {
        return $this
            ->belongsTo("App\User", "user_id");
    }

    /**
     * Define relationship between item and the store that it belongs
     *
     * @return BelongsToMany
     */
    public function store()
    {
        return $this
            ->belongsToMany("App\Store", "store_item", "item_id", "store_id");
    }
}
