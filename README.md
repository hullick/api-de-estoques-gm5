# Api de Estoques GM5

WebAPI Restfull para gerenciamento de estoques.

## Instalação

Seguir os passos de instalação do docker para [Windows](https://docs.docker.com/docker-for-windows/install/), [MAC](https://docs.docker.com/docker-for-mac/install/) ou linux nas distribuições [Debian](https://docs.docker.com/install/linux/docker-ce/debian/), [CentOS](https://docs.docker.com/install/linux/docker-ce/centos/), [Fedora](https://docs.docker.com/install/linux/docker-ce/fedora/) ou [Ubuntu](https://docs.docker.com/install/linux/docker-ce/ubuntu/).

Após, instalar a ferramenta [docker-compose](https://docs.docker.com/compose/install/), que possui na mesma página os passos para instalar para cada sistema operacional.

Depois de instalados estes dois items, executar o comando ```docker-compose up -d```, que irá levantar duas máquinas, sendo estas:

* Servidor debian 9 com apache2, que criará um tunel entre sua máquina física e esta máquina virtual. A porta ocupada na sua máquina será a 32500, apontando para a 3000 da máquina virtual, onde será executada a aplicação.

* Servidor com banco de dados MariaDB, com tunel entre sua máquina e esta máquina virtual. A porta ocupada na sua máquina será a 32501, apontando para a porta 3306 da máquina virtual. Os acessos ao banco de dados serão dados pelo usuário ```storage_system``` e senha ```5t0r4g3```, através do banco de dados ```storage```

## Aplicação

### Logs

Para auxiliar em possíveis debugs da aplicação, foram criados arquivos de log, sendo mais preciso os arquivos connections, onde encontram-se todos os acessos na aplicação, e queries, onde constam as execuções de comandos no banco de dados, estando estes dentro da pasta ```storage/logs```.

### Middlewares

Para separar a lógica dos controllers e permitir um melhor reaproveitamento de código e desacoplamento das restrições dos controllers, criei middlewares responsaveis por verificar se itens críticos constavam no cabeçalho da requisição (CheckHeaders), se o token da api foi passada (CheckApiToken), e se o json passada para a requisição é válido (CheckValidJsonData), também sendo criado um para registrar acessos na aplicação (Logging)


### Premissas

Todas as requisições devem conter, em seu cabeçalho, o item ```Content-Type``` com valor ```application/json```, informando que as requisições serão feitas utilizando dados com encode para json.

Todas as requisições deverão conter em seus dados um json válido, ainda que vazio.

Todas as requisições deverão ser realizadas por método HTTP Post.

Todos os tokens da api são válidos por 30 minutos, a menos que seja revalidado.

Algumas requisições, tipadas na sessão de rotas nesse arquivo com ```*```, necessitam do item ```api_token```, com o valor gerado pela rota ```/api/auth/login```.

### Rotas

Todas as rotas válidas para a api são prefixadas com ```/api```


```/api/auth/signup``` - Rota para registro de usuário. Retornará um objeto contendo o status da requisição, uma mensagem padrão e o atributo data, que conterá o token de acesso do usuário na api. Devem ser informados os atributos ```name```, ```password``` e ```email```. Exemplo utilizando o comando curl do linux: ```curl -X POST -H 'Content-Type: application/json' -i http://localhost:32500/api/auth/signup --data '{"name": "José das Couves", "email":"jose.couves@teste.com", "password":"j0s3"}'```. 

```/api/auth/login``` - Rota para autenticação de usuário. Retornará um objeto contendo o status da requisição, uma mensagem padrão e o atributo data, que conterá o token de acesso do usuário na api. Devem ser informados os atributos ```password``` e ```email```. Exemplo utilizando o comando curl do linux: ```curl -X POST -H 'Content-Type: application/json' -i http://localhost:32500/api/auth/login --data '{"email":"jose.couves@teste.com", "password":"j0s3"}'```. 

```/api/auth/logout``` * - Rota para desautenticação de usuário. Retornará um objeto contendo o status da requisição, uma mensagem padrão e o atributo data, que estará vazio. Não precisa ser informado nenhum atributo. Exemplo utilizando o comando curl do linux: ```curl -X POST -H 'Content-Type: application/json' -H 'api_token: [SEU TOKEN GERADO]' -i http://localhost:32500/api/auth/logout --data '{}'```.

```/api/auth/refresh``` * - Rota para revalidar o token de acesso a api do usuário. Retornará um objeto contendo o status da requisição, uma mensagem padrão e o atributo data, que estará vazio. Não precisa ser informado nenhum atributo. Exemplo utilizando o comando curl do linux: ```curl -X POST -H 'Content-Type: application/json' -H 'api_token: [SEU TOKEN GERADO]' -i http://localhost:32500/api/auth/refresh --data '{}'```.

```/api/store/register``` * - Rota para registrar uma ou mais novas loja. Retornará um objeto contendo o status da requisição, uma mensagem padrão e o atributo data, que retornará os dados das lojas. Devem ser informados os atributos ```name```, para cada objeto. Pode ser passado somente um registro de loja, quanto uma lista dos mesmos. As informações devem vir dentro de objetos no atributo ```data``` nos dados da requisição. Exemplo utilizando o comando curl do linux para uma loja apenas: ```curl -X POST -H 'Content-Type: application/json' -H 'api_token: [SEU TOKEN]' -i http://localhost:32500/api/store/register --data '{"data":{"name": "Lojas Natal"}}'```. Exemplo para mais de uma loja: ```curl -X POST -H 'Content-Type: application/json' -H 'api_token: [SEU TOKEN]' -i http://localhost:32500/api/store/register --data '{"data":[{"name": "Lojas Natal"}, {"name": "Lojas Verão"}]}'```.

```/api/store/update``` * - Rota para atualizar uma ou mais novas loja. Retornará um objeto contendo o status da requisição, uma mensagem padrão e o atributo data, que retornará os dados das lojas. Devem ser informados os atributos ```name``` e ```id```, para cada objeto. Pode ser passado somente um registro de loja, quanto uma lista dos mesmos. As informações devem vir dentro de objetos no atributo ```data``` nos dados da requisição. Exemplo utilizando o comando curl do linux para uma loja apenas: ```curl -X POST -H 'Content-Type: application/json' -H 'api_token: [SEU TOKEN]' -i http://localhost:32500/api/store/update --data '{"data":{"name": "Lojas Natal1", "id": 1}}'```. Exemplo para mais de uma loja: ```curl -X POST -H 'Content-Type: application/json' -H 'api_token: [SEU TOKEN]' -i http://localhost:32500/api/store/update --data '{"data":[{"name": "Lojas Natal1", "id": 1}, {"name": "Lojas Verão1", "id": 2}]}'```.

```/api/store/delete``` * - Rota para remover uma ou mais novas loja. Retornará um objeto contendo o status da requisição, uma mensagem padrão e o atributo data, que vai estar vazio. Devem ser informados os atributos ```id```, para cada objeto. Pode ser passado somente um registro de loja, quanto uma lista dos mesmos. As informações devem vir dentro de objetos no atributo ```data``` nos dados da requisição. Exemplo utilizando o comando curl do linux para uma loja apenas: ```curl -X POST -H 'Content-Type: application/json' -H 'api_token: [SEU TOKEN]' -i http://localhost:32500/api/store/delete --data '{"data":{"id": 1}}'```. Exemplo para mais de uma loja: ```curl -X POST -H 'Content-Type: application/json' -H 'api_token: [SEU TOKEN]' -i http://localhost:32500/api/store/delete --data '{"data":[{"id": 1}, {"id": 2}]}'```.

```/api/store/list``` * - Rota para listar lojas registradas. Retornará um objeto contendo o status da requisição, uma mensagem padrão e o atributo data, que retornará todas as lojas e os items associados a ela. Não necessita que seja passado nenhum atributo. Exemplo utilizando o comando curl do linux: ```curl -X POST -H 'Content-Type: application/json' -H 'api_token: [SEU TOKEN]' -i http://localhost:32500/api/store/list --data '{}'```.

```/api/store/associate-item``` * - Rota para associar um ou mais item a lojas. Retornará um objeto contendo o status da requisição, uma mensagem padrão e o atributo data, que retornará todas as lojas e os items associados a ela. Devem ser informados os atributos ```store_id``` e ```item_id```, para cada objeto. Exemplo utilizando o comando curl do linux: ```curl -X POST -H 'Content-Type: application/json' -H 'api_token: [SEU TOKEN]' -i http://localhost:32500/api/store/associate-item --data '{"data": {"store_id": 1, "item_id": 1"}}'```. Exemplo para mais de uma associação: ```curl -X POST -H 'Content-Type: application/json' -H 'api_token: [SEU TOKEN]' -i http://localhost:32500/api/store/associate-item --data '{"data": [{"store_id": 1, "item_id": 1"}, {"store_id": 1, "item_id": 2}, {"store_id": 2, "item_id": 1}]}'```

```/api/store/desassociate-item``` * - Rota para desassociar um ou mais item a lojas. Retornará um objeto contendo o status da requisição, uma mensagem padrão e o atributo data, que retornará todas as lojas e os items associados a ela. Devem ser informados os atributos ```store_id``` e ```item_id```, para cada objeto. Exemplo utilizando o comando curl do linux: ```curl -X POST -H 'Content-Type: application/json' -H 'api_token: [SEU TOKEN]' -i http://localhost:32500/api/store/desassociate-item --data '{"data": {"store_id": 1, "item_id": 1"}}'```. Exemplo para mais de uma associação: ```curl -X POST -H 'Content-Type: application/json' -H 'api_token: [SEU TOKEN]' -i http://localhost:32500/api/store/desassociate-item --data '{"data": [{"store_id": 1, "item_id": 1}, {"store_id": 1, "item_id": 2}, {"store_id": 2, "item_id": 1}]}'```

```/api/store/register-stored-item-quantity``` * - Rota para registrar a quantidade de items em uma ou mais lojas. Retornará um objeto contendo o status da requisição, uma mensagem padrão e o atributo data, que retornará as lojas, os items associados e a quantidade registrada. Devem ser informados os atributos ```store_id```, ```item_id``` e ```quantity```, para cada objeto. Exemplo utilizando o comando curl do linux: ```curl -X POST -H 'Content-Type: application/json' -H 'api_token: [SEU TOKEN]' -i http://localhost:32500/api/store/register-stored-item-quantity --data '{"data": {"store_id": 1, "item_id": 1, "quantity": 10}}'```. Exemplo para mais de uma associação: ```curl -X POST -H 'Content-Type: application/json' -H 'api_token: [SEU TOKEN]' -i http://localhost:32500/api/store/desassociate-item --data '{"data": [{"store_id": 1, "item_id": 1, "quantity": -5}, {"store_id": 1, "item_id": 2", "quantity": 20}}]}'```

```/api/item/register``` * - Rota para registrar uma ou mais items. Retornará um objeto contendo o status da requisição, uma mensagem padrão e o atributo data, que retornará os dados do item. Devem ser informados os atributos ```name``` e ```value```, para cada objeto. Pode ser passado somente um registro de item, quanto uma lista dos mesmos. As informações devem vir dentro de objetos no atributo ```data``` nos dados da requisição. Exemplo utilizando o comando curl do linux para um item penas: ```curl -X POST -H 'Content-Type: application/json' -H 'api_token: [SEU TOKEN]' -i http://localhost:32500/api/item/register --data '{"data":{"name": "Camisa Polo", "value": 10}}'```. Exemplo para mais de um item: ```curl -X POST -H 'Content-Type: application/json' -H 'api_token: [SEU TOKEN]' -i http://localhost:32500/api/item/register --data '{"data":[{"name": "Camisa polo", "value": 10}, {"name": "Calça jeans", "value": 69.90}]}'```.

```/api/item/update``` * - Rota para atualizar uma ou mais items. Retornará um objeto contendo o status da requisição, uma mensagem padrão e o atributo data, que retornará os dados dos items. Devem ser informados os atributos ```name```, ```value``` e ```id```, para cada objeto. Pode ser passado somente um registro de item, quanto uma lista dos mesmos. As informações devem vir dentro de objetos no atributo ```data``` nos dados da requisição. Exemplo utilizando o comando curl do linux para um item apenas: ```curl -X POST -H 'Content-Type: application/json' -H 'api_token: [SEU TOKEN]' -i http://localhost:32500/api/item/update --data '{"data":{"name": "Camisa POLO", "id": 1, "value": 10"}}'```. Exemplo para mais de um item: ```curl -X POST -H 'Content-Type: application/json' -H 'api_token: [SEU TOKEN]' -i http://localhost:32500/api/item/update --data '{"data":[{"name": "Camisa POP", "id": 1, "value": 10}, {"name": "Calça JEANS", "id": 2, "value": 92.99}]}'```.

```/api/item/delete``` * - Rota para remover uma ou mais items. Retornará um objeto contendo o status da requisição, uma mensagem padrão e o atributo data, que vai estar vazio. Devem ser informados os atributos ```id```, para cada objeto. Pode ser passado somente um registro de item, quanto uma lista dos mesmos. As informações devem vir dentro de objetos no atributo ```data``` nos dados da requisição. Exemplo utilizando o comando curl do linux para uma loja apenas: ```curl -X POST -H 'Content-Type: application/json' -H 'api_token: [SEU TOKEN]' -i http://localhost:32500/api/item/delete --data '{"data":{"id": 1}}'```. Exemplo para mais de uma loja: ```curl -X POST -H 'Content-Type: application/json' -H 'api_token: [SEU TOKEN]' -i http://localhost:32500/api/item/delete --data '{"data":[{"id": 1}, {"id": 2}]}'```.

```/api/item/list``` * - Rota para remover uma ou mais novas loja. Retornará um objeto contendo o status da requisição, uma mensagem padrão e o atributo data, que retornará todos items. Não necessita que seja passado nenhum atributo. Exemplo utilizando o comando curl do linux: ```curl -X POST -H 'Content-Type: application/json' -H 'api_token: [SEU TOKEN]' -i http://localhost:32500/api/item/list --data '{}'```.