<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StoreItemCounter extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("store_item_counter", function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->integer('quantity')
                ->nullable(false);

            $table->unsignedBigInteger("store_item_id")
                ->nullable(false);

            $table->unsignedBigInteger("user_id")
                ->nullable(false);

            $table->foreign('store_item_id')
                ->references('id')->on('store_item')
                ->onUpdate("cascade")
                ->onDelete("cascade");

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onUpdate("cascade")
                ->onDelete("cascade");

            $table->timestamp("created_at")
                ->nullable()
                ->useCurrent();

            $table->timestamp("updated_at")
                ->nullable()
                ->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
