<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StoreItem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("store_item", function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger("store_id")
                ->nullable(false);

            $table->unsignedBigInteger("item_id")
                ->nullable(false);

            $table->foreign('store_id')
                ->references('id')->on('store')
                ->onUpdate("cascade")
                ->onDelete("cascade");

            $table->foreign('item_id')
                ->references('id')->on('item')
                ->onUpdate("cascade")
                ->onDelete("cascade");

            $table->timestamp("created_at")
                ->nullable()
                ->useCurrent();

            $table->timestamp("updated_at")
                ->nullable()
                ->useCurrent();

            $table->unique(['store_id', "item_id"]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
