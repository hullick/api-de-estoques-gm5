<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Store extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("store", function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string("name")
                ->nullable(false);

            $table->unsignedBigInteger('user_id');

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onUpdate("cascade")
                ->onDelete("cascade");

            $table->timestamp("created_at")
                ->nullable()
                ->useCurrent();

            $table->timestamp("updated_at")
                ->nullable()
                ->useCurrent();

            $table->unique('name');
        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
