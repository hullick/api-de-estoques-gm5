<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AccessToken extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("access_token", function (Blueprint $table) {
            $table->string('api_token', 80)
                ->primary()
                ->unique()
                ->nullable(false);

//            Max duration for token
            $table->unsignedBigInteger("max_duration")
                ->default(60 * 30);

            $table->timestamp("created_at")
                ->nullable()
                ->useCurrent();

            $table->timestamp("updated_at")
                ->nullable()
                ->useCurrent();

            $table->unsignedBigInteger('user_id');

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onUpdate("cascade")
                ->onDelete("cascade");
        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
